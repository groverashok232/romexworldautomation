﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RomexWorldAutomation.Utilities;

namespace RomexWorldAutomation.PageObjects
{
   public class LoginPageObjects
    {
       static By userNameTextbox = By.Id("logSA_UserName");
       static By passwordTextbox = By.Id("logSA_Password");
       static By loginButton = By.Id("logSA_LoginButton");
       static By errorMessageDetails = By.CssSelector("td.Failure");
       public static void EnterLoginDetails(string userName,string password)
       {
           userNameTextbox.EnterText(userName);
           passwordTextbox.EnterText(password);
           loginButton.Click();
       }

       public static void VerifyErrorMessage(string errorMessage)
       {
          string text= errorMessageDetails.GetText();
          text.AssertShouldBe(errorMessage);
       }

       public static void VerifyLoginPageDisplayed()
       {
           userNameTextbox.AssertExists();
       }
    }
}
