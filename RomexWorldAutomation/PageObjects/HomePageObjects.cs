﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RomexWorldAutomation.Utilities;

namespace RomexWorldAutomation.PageObjects
{
   public class HomePageObjects
    {
       static By logoutLink = By.Id("MainPlaceHolder_btnLogout");
       static By administrationLink = By.CssSelector("a[onclick='ShowAdmin();']");
       static By administrationMainPopUpFrame = By.Id("ifrAdmin");
       static By administrationMiddlePopUpFrame = By.Id("ifmContent");
       static By administrationRightSidePopUpFrame = By.Id("ifrCustomization");
       static By administrationPopUpNewButton = By.Id("MainPlaceHolder_btnNew");

       static By settingsGearButton = By.CssSelector("ul[id='mapOptionsMenu']>li>a"); 
       static By settingsPopUpEmployeeTab = By.CssSelector("li[id='employeesLayerTab']");

       static By helpLink = By.Id("liHelp");
       static By helpPopUp = By.Id("helpPaneWrapper");
       static By helpPopUpClose = By.Id("helpPaneClose");

       static By errorList = By.Id("imgError");
       static By errorPopUpElement = By.Id("divNoExceptions");
       static By errorPopUpCloseButton = By.Id("MainPlaceHolder_btnCancel");

       static By editButton = By.Id("detailsPaneEdit");
       static By editPopUpWorkingHoursButton = By.Id("MainPlaceHolder_workingHoursButton");
       static By editPopUpCloseButton = By.CssSelector("a[onclick='CloseMe();']");

       static By employeesTab = By.Id("employeesTabButton");
       static By locationsTab = By.Id("sitesTabButton");
       static By zonesTab = By.Id("zoneTabButton");
       static By detailsTab = By.Id("liDetailsPaneDetails");
       static By statisticsTab = By.Id("liDetailsPaneStatistics");
       static By mapTab = By.Id("liMap");
       static By alertsTab = By.Id("liAlerts");
       static By jobsTab = By.Id("liJobs");
       static By reportsTab = By.Id("liReports");
       

       public static void ClickLogoutLink()
       {
           logoutLink.Click();
       }

       public static void VerifyHomePageDisplayed()
       {
           logoutLink.AssertExists();
       }

       public static void ClickAdministrationLink()
       {
           administrationLink.Click();
       }

       public static void VerifyAdministrationPopUpDisplayed()
       {
           administrationMainPopUpFrame.SwitchToIFrame();
           administrationMiddlePopUpFrame.SwitchToIFrame();
           administrationRightSidePopUpFrame.SwitchToIFrame();
           administrationPopUpNewButton.AssertExists();
       }

       public static void ClickSettingsGearButton()
       {
           settingsGearButton.Click();
       }

       public static void ChangeFrameForSettingsPopUpWindow()
       {
           SelectingBrowsers.driver.SwitchTo().Frame(4);
       }
       public static void VerifySettingsPopUpDisplayed()
       {      
           settingsPopUpEmployeeTab.AssertExists();
       }

       public static void ClickHelpLink()
       {
           helpLink.Click();
       }

       public static void VerifyHelpPopUpDisplayed()
       {
           helpPopUp.AssertExists();

       }

       public static void HelpLinkClose()
       {

           helpPopUpClose.Click();
       }

       public static void CLickErrorList()
       {
           errorList.Click();
       }

       public static void ChangeFrameForErrorsPopUpWindow()
       {
           SelectingBrowsers.driver.SwitchTo().Frame(4);
       }

       public static void VerifyErrorListPopUp()
       {
           errorPopUpElement.AssertExists();
       }
       public static void ClickErrorPopUpCloseButton()
       {
           errorPopUpCloseButton.Click();
       }

       public static void ClickEditButton()
       {
           editButton.Click();
       }

       public static void ChangeFrameForEditPopUpWindow()
       {
           SelectingBrowsers.driver.SwitchTo().Frame(4);
       }

       public static void VerifyEditPopUpDisplayed()
       {
           editPopUpWorkingHoursButton.AssertExists();
       }
       public static void ClickEditPopUpCloseButton()
       {
           editPopUpCloseButton.Click();
       }

       public static void VerifyEmployeesTabDisplayed()
       {

           employeesTab.AssertExists();
       }

       public static void VerifyLocationsTabDisplayed()
       {
           locationsTab.AssertExists();

       }

       public static void VerifyZonesTabDisplayed()
       {
           zonesTab.AssertExists();
       }

       public static void VerifyDetailsTabDisplayed()
       {
           detailsTab.AssertExists();
       }

       public static void VerifyStatisticsTabDisplayed()
       {
           statisticsTab.AssertExists();
       }

       public static void VerifyMapsTabDisplayed()
       {
           mapTab.AssertExists();
       }

       public static void VerifyAlertTabDisplayed()
       {
           alertsTab.AssertExists();
       }

       public static void VerifyJobsTabDisplayed()
       {
           jobsTab.AssertExists();
       }

       public static void VerifyReportTabDisplayed()
       {
           reportsTab.AssertExists();
       }
    }
}
