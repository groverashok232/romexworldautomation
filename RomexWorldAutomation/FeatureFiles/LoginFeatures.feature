﻿Feature: LoginFeatures
	Background: start the browser
	Given User has open the romex browser 

@mytag
Scenario Outline: User can able to test login scenarios
	 Given User can able to enter login details as per <Scenario>
	 Then User can able to perform following actions as per <Scenario>
	 Examples: 
	 | Scenario            |
	 | Valid Credentials   |
	 | Invalid Credentials |

@mytag
Scenario: User can able to test logout scenario
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Logout button
	 Then User can able to logout successfully