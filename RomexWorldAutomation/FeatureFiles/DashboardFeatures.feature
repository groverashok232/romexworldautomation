﻿Feature: DashboardFeatures 
	Background: start the browser
	Given User has open the romex browser 
	 
@mytag
Scenario: User can able to verify the Adminstration window
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Administration link on dashboard
	 Then It opens administration window and new button displayed

@mytag
Scenario: User can able to verify the Settings(Gear) window
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Setting button on dashboard
	 Then It opens Setting window and Employee Tab displayed

@mytag
Scenario: User can able to verify the Help window
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Help link on dashboard
	 Then It opens Small popup on right side of window
	 And User should able to click the close button

@mytag
Scenario: User can able to verify the Error List window
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Error List button on dashboard
	 Then It opens Errors popup window
	 And User should able to click the Error PopUp close button

@mytag
Scenario: User can able to verify the Edit window
	 Given User can able to enter login details as per Valid Credentials
	 When User clicks on Edit button on dashboard
	 Then It opens user profile window where it can modify the details
	 And User should able to click the Edit pop up cancel button

@mytag
Scenario: User can able to verify all the tabs display
	 Given User can able to enter login details as per Valid Credentials
	 Then User can able to verify Employees, Locations, Zones, Details, Statistics, Maps, Alert, Jobs, Report should be displayed