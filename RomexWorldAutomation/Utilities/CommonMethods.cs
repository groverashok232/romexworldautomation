﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RomexWorldAutomation.Utilities
{
   public static class CommonMethods
    {
        public static void EnterText(this By objectName, string text)
        {
            SelectingBrowsers.driver.FindElement(objectName).SendKeys("");
            SelectingBrowsers.driver.FindElement(objectName).SendKeys(text);

        }

        public static void PressKey(this By objectName, string key)
        {
            SelectingBrowsers.driver.FindElement(objectName).SendKeys(key);
        }

        public static void Click(this By objectName)
        {
            Thread.Sleep(1000);
            SelectingBrowsers.driver.FindElement(objectName).Click();
        }

        public static void Click(this IWebElement objectName)
        {
            Thread.Sleep(500);
            objectName.Click();
        }

        public static void Clear(this By objectName)
        {
            SelectingBrowsers.driver.FindElement(objectName).Clear();
        }
        public static void SelectTextInDropdown(this By objectName, string value)
        {
            SelectElement select = new SelectElement(SelectingBrowsers.driver.FindElement(objectName));
            select.SelectByText(value);
        }

        public static void SelectValueInDropdown(this By objectName, string value)
        {
            SelectElement select = new SelectElement(SelectingBrowsers.driver.FindElement(objectName));
            select.SelectByValue(value);
        }



        public static void DoubleClick(this IWebElement objectName)
        {
            Actions actio = new Actions(SelectingBrowsers.driver);
            actio.DoubleClick(objectName).Perform();
        }

        public static void AssertTitle(string title)
        {
            Assert.Equals(SelectingBrowsers.driver.Title, title);
        }

        public static void AssertExists(this By objectName)
        {
            IWebElement element = SelectingBrowsers.driver.FindElement(objectName);

            Assert.IsTrue(element.Displayed, element + " is not getting displayed on the webpage");
        }

        public static void AssertValueContains(this By objectName, string value)
        {
            IWebElement element = SelectingBrowsers.driver.FindElement(objectName);

            Assert.IsTrue(element.Text.Contains(value), element + " is not getting displayed on the webpage");
        }


        public static void ClickHyperlinkByLinkText(this string objectName)
        {
            SelectingBrowsers.driver.FindElement(By.LinkText(objectName)).Click();
        }

        public static string GetText(this By objectName)
        {
            return SelectingBrowsers.driver.FindElement(objectName).Text;
        }

        public static void ClickControlInsideHoverByChildCSS(this By childObjectName, string parentObjectName)
        {
            Actions actio = new Actions(SelectingBrowsers.driver);
            actio.MoveToElement(SelectingBrowsers.driver.FindElement(By.LinkText(parentObjectName))).MoveToElement(SelectingBrowsers.driver.FindElement(childObjectName)).Click().Build().Perform();
        }

        public static void JavaScriptScrollTillEnd()
        {
            IJavaScriptExecutor js = SelectingBrowsers.driver as IJavaScriptExecutor;
            js.ExecuteScript("javascript:window.scrollBy(0,document.body.scrollHeight-150)");
        }

        public static void AssertShouldNotBe(this string oneValue, string otherValue)
        {
            Assert.AreNotEqual(oneValue, otherValue, oneValue + " is matching with the " + otherValue);

        }

        public static void AssertShouldBe(this string oneValue, string otherValue)
        {
            Assert.AreEqual(oneValue, otherValue, oneValue + " is not matching with the " + otherValue);

        }

        public static void PressEnterKey(this By objectName)
        {
            SelectingBrowsers.driver.FindElement(objectName).SendKeys(Keys.Enter);

        }

        public static void HandlePopUp()
        {
            Thread.Sleep(1000);
            IAlert alert = SelectingBrowsers.driver.SwitchTo().Alert();
            Thread.Sleep(3000);
            alert.Accept();
        }

        public static void SwitchToIFrame(this By objectName)
        {
            SelectingBrowsers.driver.SwitchTo().Frame(SelectingBrowsers.driver.FindElement(objectName));
        }

        public static void SwitchToMainWindow()
        {
            SelectingBrowsers.driver.SwitchTo().DefaultContent();
        }

    }
}
