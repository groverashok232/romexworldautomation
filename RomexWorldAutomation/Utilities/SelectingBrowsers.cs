﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomexWorldAutomation.Utilities
{
   public class SelectingBrowsers
    {
        public IWebDriver OpenBrowser(string browserName, string url)
        {
            switch (browserName)
            {
                case "Firefox":
                    driver = new FirefoxDriver();
                    driver.Navigate().GoToUrl(url);
                    driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));
                    driver.Manage().Window.Maximize();
                    break;
                case "IE":
                    //driver = new FirefoxDriver();
                    //driver.Navigate().GoToUrl(GlobalVariables.browserName);
                    //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));
                    //driver.Manage().Window.Maximize();
                    break;
                case "Chrome":
                    driver = new ChromeDriver();
                    driver.Navigate().GoToUrl(url);
                    driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));
                    driver.Manage().Window.Maximize();
                    break;
            }
            return driver;
        }

        public static IWebDriver driver
        {
            get;
            set;
        }
    }
}
