﻿using RomexWorldAutomation.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace RomexWorldAutomation.PageStepsDefinition
{
    [Binding]
    public class HomePageSteps
    {
        [When(@"User clicks on Administration link on dashboard")]
        public void WhenUserClicksOnAdministrationLinkOnDashboard()
        {
            HomePageObjects.ClickAdministrationLink();
        }
       
        [Then(@"It opens administration window and new button displayed")]
        public void ThenItOpensAdministrationWindowAndNewButtonDisplayed()
        {
            HomePageObjects.VerifyAdministrationPopUpDisplayed();
        }

        [When(@"User clicks on Setting button on dashboard")]
        public void WhenUserClicksOnSettingButtonOnDashboard()
        {
            HomePageObjects.ClickSettingsGearButton();
        }

        [Then(@"It opens Setting window and Employee Tab displayed")]
        public void ThenItOpensSettingWindowAndEmployeeTabDisplayed()
        {
            HomePageObjects.ChangeFrameForSettingsPopUpWindow();
            HomePageObjects.VerifySettingsPopUpDisplayed();
        }

        [When(@"User clicks on Help link on dashboard")]
        public void WhenUserClicksOnHelpLinkOnDashboard()
        {
            HomePageObjects.ClickHelpLink();             
        }

        [Then(@"It opens Small popup on right side of window")]
        public void ThenItOpensSmallPopupOnRightSideOfWindow()
        {
            HomePageObjects.VerifyHelpPopUpDisplayed();

        }

        [Then(@"User should able to click the close button")]
        public void ThenUserShouldAbleToClickTheCloseButton()
        {
            HomePageObjects.HelpLinkClose();
        }

        [When(@"User clicks on Error List button on dashboard")]
        public void WhenUserClicksOnErrorListButtonOnDashboard()
        {
            HomePageObjects.CLickErrorList();
        }

        [Then(@"It opens Errors popup window")]
        public void ThenItOpensErrorsPopupWindow()
        {
            HomePageObjects.ChangeFrameForErrorsPopUpWindow();
            HomePageObjects.VerifyErrorListPopUp();
        }

        [Then(@"User should able to click the Error PopUp close button")]
        public void ThenUserShouldAbleToClickTheErrorPopUpCloseButton()
        {
            HomePageObjects.ClickErrorPopUpCloseButton();
        }

        [When(@"User clicks on Edit button on dashboard")]
        public void WhenUserClicksOnEditButtonOnDashboard()
        {
            HomePageObjects.ClickEditButton();
        }

        [Then(@"It opens user profile window where it can modify the details")]
        public void ThenItOpensUserProfileWindowWhereItCanModifyTheDetails()
        {
            HomePageObjects.ChangeFrameForEditPopUpWindow();
            HomePageObjects.VerifyEditPopUpDisplayed();
        }

        [Then(@"User should able to click the Edit pop up cancel button")]
        public void ThenUserShouldAbleToClickTheEditPopUpCancelButton()
        {
            HomePageObjects.ClickEditPopUpCloseButton();
        }

        [Then(@"User can able to verify Employees, Locations, Zones, Details, Statistics, Maps, Alert, Jobs, Report should be displayed")]
        public void ThenUserCanAbleToVerifyEmployeesLocationsZonesDetailsStatisticsMapsAlertJobsReportShouldBeDisplayed()
        {
            HomePageObjects.VerifyEmployeesTabDisplayed();
            HomePageObjects.VerifyLocationsTabDisplayed();
            HomePageObjects.VerifyZonesTabDisplayed();
            HomePageObjects.VerifyDetailsTabDisplayed();
            HomePageObjects.VerifyStatisticsTabDisplayed();
            HomePageObjects.VerifyMapsTabDisplayed();
            HomePageObjects.VerifyAlertTabDisplayed();
            HomePageObjects.VerifyJobsTabDisplayed();
            HomePageObjects.VerifyReportTabDisplayed();

        }

    }
}
