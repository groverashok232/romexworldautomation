﻿using RomexWorldAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace RomexWorldAutomation.PageStepsDefinition
{
    [Binding]
    public class CommonSteps : SelectingBrowsers
    {

        [Given(@"User has open the romex browser")]
        public void GivenUserHasOpenTheRomexBrowser()
        {
            driver = OpenBrowser(GlobalVariables.browserUsed, GlobalVariables.romexForms);
        }
         
        [AfterScenario]
        public void CloseBrowser()
        {
            driver.Quit();
        }
    }
}
