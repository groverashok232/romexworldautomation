﻿using RomexWorldAutomation.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace RomexWorldAutomation.PageStepsDefinition
{
    [Binding]
   public class LoginPageSteps
    {
        [Given(@"User can able to enter login details as per (.*)")]
        public void GivenUserCanAbleToEnterLoginDetailsAsPerValidCredentials(string scenario)
        {
         switch(scenario)
         {
             case "Valid Credentials":
                 LoginPageObjects.EnterLoginDetails("groverashok232@gmail.com", "654321");
                 Thread.Sleep(3000);
                 break;

             case "Invalid Credentials":
                 LoginPageObjects.EnterLoginDetails("asddf@gmail.com", "654321");
                 LoginPageObjects.VerifyErrorMessage("Please try again");
                 break;

             default:
                 break;
         }
        }
       
        [Then(@"User can able to perform following actions as per (.*)")]
        public void ThenUserCanAbleToPerformFollowingActionsAsPerValidCredentials(string scenario)
        {
            switch (scenario)
            {
                case "Valid Credentials":
                    HomePageObjects.VerifyHomePageDisplayed();
                    break;

                case "Invalid Credentials":                 
                    LoginPageObjects.VerifyErrorMessage("Please try again");
                    break;
                default:
                    break;
            }
        }

        [When(@"User clicks on Logout button")]
        public void WhenUserClicksOnLogoutButton()
        {
            Thread.Sleep(3000);
            HomePageObjects.ClickLogoutLink();
        }

        [Then(@"User can able to logout successfully")]
        public void ThenUserCanAbleToLogoutSuccessfully()
        {
            LoginPageObjects.VerifyLoginPageDisplayed();
        }

    }
}
